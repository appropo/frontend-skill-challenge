## Tasks
1. Clone this repository for local development
2. Create a small time-tracking application with HTML, CSS and JS
3. Create a pull request for your final code

## Minimum requirements for the application
- Start and stop functionality
- Possibility to add a description for each time-log
- Display the saved time-logs in a list

## Additional information
- The choice for the JS library/framework is up to you
- Feel free to use a bootstrap framework for HTML/CSS

